# Course Scheduling Application

> Template provided for the students of the [Bottega Code School](https://bottega.tech/) and is a *fork from [es6-webpack2-starter](https://github.com/micooz/es6-webpack2-starter).*

This project is a simple class scheduling application.  While it is mostly finished, there might be a few bugs hidden away here and there.

A live version of this app can be found [here](https://classscheduler.onrender.com/).